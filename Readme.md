**Install Jhipster**

1. Install Java 11. We recommend you use [Eclipse Temurin](https://adoptium.net/temurin/releases/?version=11) builds, as they are open source and free.
2. Install Node.js from the [Node.js website](https://nodejs.org/en/) (please use an LTS 64-bit version, non-LTS versions are not supported)
3. Install docker and docker-compose via https://docs.docker.com/get-docker
4. Install JHipster: `npm install -g generator-jhipster@7.8.1` (you can also try the latest version, but it's giving problems on Mac)

**Create your first application**

1. Create application: `jhipster`
2. Select the following answers:

- Monolythic application
- Name: your choice
- Webflux: No
- Default package: your choice
- JWT for auth
- SQL for db
- Postgres
- H2 with diskbased persistence
- No cache
- Maven
- Elastic search (for a simpler, but less rich, application, don't select Elastic Search)
- Angular
- Do generate admin UI
- Theme: your choice
- Internationalization: yes (select your own languages)
- UAT: your choice
- Other generators: no

**Add entities**

1. Add whatever entities you like with: `jhipster entity your-entity-name` (Jhipster will ask your questions)
2. Answer questsion with: 

- Yes, generate a separate service class
- Yes, generate a DTO with Mapstruct
- Dynamic filtering for the entities with JPA Static metamodel 
- Read-only: no
- Pagination: Yes, with infinite scroll and sorting headers 
- y or a for all subsequent questions

**Run application (simple)**

Prequisite: this doesn't work if you selected Elastic Search as a search engine

1. Run application with `./mvnw`
2. Wait for maven to download the internet
3. Open application at http://localhost:8080

**Run application**

1. Build application (for docker) with: `npm run java:docker`
2. Wait for maven to download the internet
3. Run application with `docker-compose -f src/main/docker/app.yml up -d` (sometimes startup of Elastic Search is too slow, then you need to run this command twice)
3. Open application at http://localhost:8080

**Things to do / try**

1. Play around with the generated application at http://localhost:8080
2. Analyze the generated code
3. Interact with the generated backend with Postman (or any other REST tool)
3. Add your own controller, and inject generated classes into it (mind: DONT TOUCH THE GENERATED CODE)
4. Add your own code to the frontend, and inject generated classes into it (mind: DONT TOUCH THE GENERATED CODE)
5. Create a new frontend and connect it to the generated backend





 

